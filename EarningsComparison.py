from abc import ABC, abstractmethod


class Worker(ABC):
    @abstractmethod
    def earnings(self):
        pass


class PerHour(Worker):
    def __init__(self):
        self.numberOfHours = 80
        self.payPerHour = 100

    def earnings(self):
        return self.numberOfHours * self.payPerHour


class Contract(Worker):
    def __init__(self):
        self.contractValue = 6000

    def earnings(self):
        return self.contractValue


perHourWorker = PerHour()
contractWorker = Contract()
perHourWorker.earnings()
contractWorker.earnings()
print("Worker paid per hour earnings ")
print(perHourWorker.earnings)
print("Worker paid monthly earnings ")
print(contractWorker.earnings)